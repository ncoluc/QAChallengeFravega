class fravegaPage {

  visitHome() {
    cy.visit('https://www.fravega.com/');
  }

  cerrarTooltipDeCostoDeEnvio() {
    cy.get('.GeolocalizationDesktop__StyledCloseIcon-wgxe4i-0').click();
  }

  realizarBusquedaProducto(producto) {
    cy.get('.InputBar__SearchInput-t6v2m1-1').type(producto);
    cy.get('.InputBar__SearchButton-t6v2m1-2').click();
  }

  clickEnFiltro(filtro) {
    cy.get('[name=subcategoryAggregation]').contains(filtro).click();
  }

  clickVerTodasMarcas() {
    cy.get('[name=viewAllBrands]').click();
  }

  realizarBusquedaMarcaEnVerTodas(marca) {
    cy.get('.styled__SearchInput-sc-37brzp-2').type(marca);
  }

  clickMarcaEnVerTodas(marca) {
    cy.get('.styled__StyledCheckbox-sc-37brzp-4.eTNIZl').children('label').contains(marca).click();
  }

  clickBotonAplicarEnMarcas() {
    cy.get('#apply').click();
  }

  seleccionarMarcaEnVerTodas(marca) {
    this.clickVerTodasMarcas();
    this.realizarBusquedaMarcaEnVerTodas(marca);
    this.clickMarcaEnVerTodas(marca);
    this.clickBotonAplicarEnMarcas();
  }

  getListaArticulosResultados() {
    return cy.get('.PieceLayout-orsj2a-0.PieceLayout__ResponsiveLayout-orsj2a-3.GKcLt');
  }

  getTitulosDeLosArticulosResultados() {
    return this.getListaArticulosResultados().children('div').children('h4');
  }

  getTotalArticulosResultados(){
    return cy.get('.listingDesktopstyled__TotalResult-wzwlr8-2 > span');
  }

  getBreadcrumItem(){
    return cy.get('[name=breadcrumb]').children('ul').children('li').children('[name=breadcrumbItem]');
  }

}

export default new fravegaPage();