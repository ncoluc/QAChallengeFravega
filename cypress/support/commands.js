// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("buscarCerveceriasPorNombreConQuery", function (nombre) {
    cy.request({
        method: 'GET',
        url: 'https://api.openbrewerydb.org/breweries/autocomplete',
        qs: { 'query': nombre }
    }).then((response) => {
        return response;
    });

});


Cypress.Commands.add("datosCerveceriaPorID", function (id) {
    cy.request('https://api.openbrewerydb.org/breweries/' + id).then((response) => {
        return response.body;
    });

});

