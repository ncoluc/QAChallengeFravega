import fravegaPage from '../support/pageObjects/fravegaPage';

describe('Tests del Challenge QA Automation', function () {

    it('TestFrontend', function () {

        fravegaPage.visitHome();

        //cierra tooltip emergente de costos de envio
        fravegaPage.cerrarTooltipDeCostoDeEnvio();

        //Ingresamos Heladera en la barra de busqueda y click en la lupa para buscar
        fravegaPage.realizarBusquedaProducto('Heladera');

        //Click en el filto Heladeras
        fravegaPage.clickEnFiltro('Heladeras');

        //Click en Ver todas las marcas
        //Ingresamos Samsung en el buscador de marcas
        //Click en Samsung
        //Click boton Aplicar
        fravegaPage.seleccionarMarcaEnVerTodas('Samsung');

        //Guardamos los titulos de los articulos resultantes
        fravegaPage.getTitulosDeLosArticulosResultados().then((articulos) => {
            const cantidadArticulos = articulos.length;

            //ASSERT. Los titulos contienen la palabra 'Samsung'
            for (let index = 0; index < cantidadArticulos; index++) {
                const valor = articulos.eq(index).text();
                expect(valor.includes('Samsung'), 'Los articulos resultantes deben contener "Samsung" en su titulo').to.be.true;
            }

            //ASSERT. La cantidad de elementos de la lista coincida con los resultados mostrando por el frontend
            fravegaPage.getTotalArticulosResultados().then((response) => {
                const valor = response.text();
                expect(valor, 'La cantidad de articulos debe ser igual al mostrado por el frontend').to.equal(cantidadArticulos.toString());
            })

        })

        //Localizamos el breadcrumbItem
        fravegaPage.getBreadcrumItem().then((response) => {
            const valor = response.text();
            //ASSERT. En el breadcrumb de la página se visualize "Heladeras con Frezzer"
            expect(valor.includes('Heladeras con Frezzer'), 'En el breadcrumb se debe visualizar "Heladeras con Frezzer"').to.be.true;
        })

    });



    it('TestBackend', function () {

        //Buscamos las cervecerias que tengan en su nombre 'lagunitas'
        cy.buscarCerveceriasPorNombreConQuery('lagunitas').then((response) => {
            response.body.forEach(cerveceria => {

                //Filtramos por las que se llaman "Lagunitas Brewing Co"
                if (cerveceria.name == "Lagunitas Brewing Co") {

                    //Obtenemos los datos de las cervecerias por id
                    cy.datosCerveceriaPorID(cerveceria.id).then((datosCerveceria) => {

                        //Filtramos por las que son del estado de "California"
                        if (datosCerveceria.state == "California") {
                            //ASSERTs
                            expect(datosCerveceria.id).to.equal(761);
                            expect(datosCerveceria.name).to.equal("Lagunitas Brewing Co");
                            expect(datosCerveceria.street).to.equal("1280 N McDowell Blvd");
                            expect(datosCerveceria.phone).to.equal("7077694495");
                        }
                    });
                }
            });
        });

    });


})